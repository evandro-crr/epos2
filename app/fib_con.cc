#include <utility/ostream.h>

using namespace EPOS;
OStream cout;
constexpr unsigned fibonacci(const unsigned x) {
  return x <= 1 ? 1 : fibonacci(x - 1) + fibonacci(x - 2);
}

int main() {
  cout<< fibonacci(44);
}


