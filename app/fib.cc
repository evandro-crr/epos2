#include <utility/ostream.h>

using namespace EPOS;
OStream cout;
template<int  N>
constexpr int fibonacci() {return fibonacci<N-1>() + fibonacci<N-2>(); }
template<>
constexpr int fibonacci<1>() { return 1; }
template<>
constexpr int fibonacci<0>() { return 0; }

int main() {
    cout << fibonacci<44>();
    return 0;
}
