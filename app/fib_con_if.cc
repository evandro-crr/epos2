#include <utility/ostream.h>

using namespace EPOS;
OStream cout;
template<int N>
int fibonacci() {
    if constexpr (N>=2)
        return fibonacci<N-1>() + fibonacci<N-2>();
    else
        return N;
    return N;
}

int main() {
    cout << fibonacci<44>();
    return 0;
}
